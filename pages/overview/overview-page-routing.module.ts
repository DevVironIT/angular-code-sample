import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewPageComponent } from './components/overview-page/overview-page.component';
import { AuthGuard } from '@auth/guards/auth.guard';
import { PortGuard } from '@ports/guards';
import { Pages } from '@shared/constants';

const routes: Routes = [
    {
        path: 'overview',
        component: OverviewPageComponent,
        canActivate: [AuthGuard, PortGuard],
        data: {
            pageName: Pages.overviewPage,
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OverviewPageRoutingModule { }
