import { Action } from '@ngrx/store';

import { Vessel } from '@vessels/models';
import { LatLngBounds } from 'leaflet';
import { Dictionary } from '@shared/models';

export enum OverviewPageActionTypes {
    SelectVessel = '[Overview Page] Select Vessel',
    DeselectVessel = '[Overview Page] Deselect Vessel',
    CenterMapOnVessel = '[Overview Page] Center Map on Vessel',
    SetMapBounds = '[Overview Page] Set Map Bounds',
    ToggleMapTool = '[Overview Page] Toggle Map Tool',
    ChangeCursorType = '[Overview Page] Change Cursor Type',
    ToggleLayerManager = '[Overview Page] Toggle Layer Manager',
    CloseLayerManager = '[Overview Page] Close Layer Manager',
    TogglePassageWidget = '[Overview Page] Toggle Passage Widget',
    SetConfirmationPopupVisibility = '[Overview Page] Set Confirmation Popup Visibility',
    LoadVesselsPassageInfo = '[Overview Page] Load Vessels Passage Info'
}

export class SelectVessel implements Action {
    readonly type = OverviewPageActionTypes.SelectVessel;

    constructor(public payload: Vessel) { }
}

export class DeselectVessel implements Action {
    readonly type = OverviewPageActionTypes.DeselectVessel;
}

export class CenterMapOnVessel implements Action {
    readonly type = OverviewPageActionTypes.CenterMapOnVessel;

    constructor(public payload: Vessel) { }
}

export class SetMapBounds implements Action {
    readonly type = OverviewPageActionTypes.SetMapBounds;

    constructor(public payload: LatLngBounds) { }
}

export class ToggleMapTool implements Action {
    readonly type = OverviewPageActionTypes.ToggleMapTool;

    constructor(public payload: string) { }
}

export class ChangeCursorType implements Action {
    readonly type = OverviewPageActionTypes.ChangeCursorType;

    constructor(public payload: Dictionary<boolean>) { }
}

export class ToggleLayerManager implements Action {
    readonly type = OverviewPageActionTypes.ToggleLayerManager;
}

export class CloseLayerManager implements Action {
    readonly type = OverviewPageActionTypes.CloseLayerManager;
}

export class TogglePassageWidget implements Action {
    readonly type = OverviewPageActionTypes.TogglePassageWidget;
}

export class SetConfirmationPopupVisibility implements Action {
    readonly type = OverviewPageActionTypes.SetConfirmationPopupVisibility;

    constructor(public payload: boolean) { }
}

export class LoadVesselsPassageInfo implements Action {
    readonly type = OverviewPageActionTypes.LoadVesselsPassageInfo;
}


export type OverviewPageActions =
    | SelectVessel
    | DeselectVessel
    | CenterMapOnVessel
    | SetMapBounds
    | ToggleMapTool
    | ChangeCursorType
    | ToggleLayerManager
    | CloseLayerManager
    | TogglePassageWidget
    | SetConfirmationPopupVisibility
    | LoadVesselsPassageInfo
    ;
