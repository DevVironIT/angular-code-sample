import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import {
    mergeMap,
    map,
    switchMap,
    withLatestFrom
} from 'rxjs/operators';

import * as fromStore from '@pages/overview/store/reducers';
import * as fromRouteStore from '@routes/store';
import * as fromVessels from '@vessels/store';
import { GeotiffActionTypes, UploadGeotiffSuccess } from '@geotiff/store';
import { OverviewPageActionTypes } from '@pages/overview/store/actions';

@Injectable()
export class OverviewPageEffects {

    constructor(
        private actions$: Actions,
        private store: Store<fromStore.State>
    ) { }


    @Effect() uploadGeotiffSuccess$: Observable<Action> = this.actions$
        .pipe(
            ofType(GeotiffActionTypes.UploadGeotiffSuccess),
            map((action: UploadGeotiffSuccess) => action.payload),
            mergeMap(tiffData => of({ type: OverviewPageActionTypes.SetMapBounds, payload: tiffData.bounds }))
        );

    @Effect() loadVesselsPassageInfo$: Observable<Action> = this.actions$
        .pipe(
            ofType(OverviewPageActionTypes.LoadVesselsPassageInfo),
            switchMap(() => {
                return [
                    new fromRouteStore.LoadAllRoutes(),
                    new fromVessels.LoadVesselsNextPassage(),
                    new fromVessels.LoadVesselsCloudEta(),
                    new fromVessels.LoadVesselsEcdisData(),
                    new fromVessels.LoadVesselsSchedule()
                ];
            })
        );
}
