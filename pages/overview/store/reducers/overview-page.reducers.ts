import { LatLngTuple, LatLngBounds, latLngBounds } from 'leaflet';

import {
    OverviewPageActions,
    OverviewPageActionTypes
} from '@pages/overview/store/actions';
import { Dictionary } from '@shared/models';

export interface State {
    selectedVesselId: number;
    mapCenter: LatLngTuple;
    mapBounds: LatLngBounds;
    activeMapTool: string;
    cursorState: Dictionary<boolean>;
    isLayerManagerVisible: boolean;
    isConfirmationPopupVisible: boolean;
}

export const initialState: State = {
    selectedVesselId: null,
    mapCenter: null,
    mapBounds: latLngBounds([[60, 60], [-60, -60]]),
    activeMapTool: null,
    cursorState: {
        zoneDrawer: false
    },
    isLayerManagerVisible: false,
    isConfirmationPopupVisible: false
};

export function reducer(state = initialState, action: OverviewPageActions): State {
    switch (action.type) {
        case OverviewPageActionTypes.SelectVessel: {
            return {
                ...state,
                selectedVesselId: action.payload.id
            };
        }
        case OverviewPageActionTypes.DeselectVessel: {
            return {
                ...state,
                selectedVesselId: null
            };
        }
        case OverviewPageActionTypes.CenterMapOnVessel: {
            if (!action.payload.freshestPosition) {
                return state;
            }

            return {
                ...state,
                mapCenter: [
                    action.payload.freshestPosition.lat,
                    action.payload.freshestPosition.lon
                ],
                mapBounds: null
            };
        }
        case OverviewPageActionTypes.SetMapBounds: {
            return {
                ...state,
                mapCenter: null,
                mapBounds: action.payload
            };
        }
        case OverviewPageActionTypes.ToggleMapTool: {
            return {
                ...state,
                activeMapTool: state.activeMapTool === action.payload ? null : action.payload
            };
        }
        case OverviewPageActionTypes.ChangeCursorType: {
            return {
                ...state,
                cursorState: {
                    ...state.cursorState,
                    ...action.payload
                }
            };
        }
        case OverviewPageActionTypes.ToggleLayerManager: {
            return {
                ...state,
                isLayerManagerVisible: !state.isLayerManagerVisible
            };
        }
        case OverviewPageActionTypes.CloseLayerManager: {
            return {
                ...state,
                isLayerManagerVisible: false
            };
        }
        case OverviewPageActionTypes.SetConfirmationPopupVisibility: {
            return {
                ...state,
                isConfirmationPopupVisible: action.payload
            };
        }
        default: {
            return state;
        }
    }
}
