import {
    createSelector,
    createFeatureSelector
} from '@ngrx/store';

import * as fromOverviewPage from './overview-page.reducers';
import * as fromRoot from '@store/index';
import * as fromVessels from '@vessels/store';
import * as fromRoutes from '@routes/store/reducers';
import { RouteStatuses } from '@routes/constants';
import { Vessel } from '@vessels/models';
import { VesselUtil } from '@vessels/utils';

export interface OverviewPageState {
    overviewPage: fromOverviewPage.State;
}

export interface State extends fromRoot.State {
    overviewPage: OverviewPageState;
}

export const reducers = {
    overviewPage: fromOverviewPage.reducer
};

const selectPageState = createFeatureSelector<OverviewPageState>('overview-page');
export const getSelectedVesselId = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.selectedVesselId);
export const getMapCenter = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.mapCenter);
export const getMapBounds = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.mapBounds);
export const getActiveMapTool = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.activeMapTool);
export const getCursorState = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.cursorState);
export const getLayerManagerState = createSelector(selectPageState, (state: OverviewPageState) => state.overviewPage.isLayerManagerVisible);
export const getConfirmationPopupVisibility = createSelector(selectPageState, (state: OverviewPageState) =>
    state.overviewPage.isConfirmationPopupVisible);

export const getMapCursorPointer = createSelector(
    getCursorState,
    cursorState => {
        return Object.keys(cursorState).some(key => !!cursorState[key]);
    }
);

export const getSelectedVesselTcmsId = createSelector(
    fromVessels.getAllVessels,
    getSelectedVesselId,
    (vessels, selectedId) => {
        if (!selectedId) {
            return null;
        }
        return vessels.find(v => v.id === selectedId).tcmsId;
    }
);

export const getSelectedVessel = createSelector(
    fromVessels.getAllVessels,
    getSelectedVesselId,
    (vessels, selectedId) => {
        return vessels.find(v => v.id === selectedId) || null;
    }
);

export const getMonitoredRoute = createSelector(
    getSelectedVessel,
    fromRoutes.getAllRoutes,
    (vessel, routes) => {
        return routes.find(
            route =>
                vessel
                && route.metaInfo.status === RouteStatuses.Monitored
                && route.metaInfo.vessel.dms_id === vessel.dmsClientId
        );
    }
);

export const getTrackBySelectedVessel = createSelector(
    getSelectedVessel,
    fromVessels.getVesselsTracks,
    (vessel, tracks) => vessel && tracks ? tracks.find(track => track.vesselId === vessel.id) : null
);

export const getVesselPopupData = createSelector(
    fromVessels.getAllVessels,
    fromVessels.getOwnAisDataEntities,
    (vessels: Vessel[], allOwnAisData, tcmsId) => {
        const vessel = vessels.find(v => v.tcmsId === tcmsId);
        return {
            ...vessel,
            ownAisData: vessel ? allOwnAisData[vessel.tcmsId] : null
        };
    }
);

export const getVesselsPassagesInfo = createSelector(
    fromVessels.getAllVessels,
    fromRoutes.getAllRoutes,
    fromVessels.getAllVesselNextPassages,
    (vessels, routes, nextPassages) =>
        vessels.map(vessel => {
            const vesselRoute = routes.find(route => route.metaInfo.vessel.dms_id === vessel.dmsClientId);
            const vesselNextPassage = nextPassages.find(passage => passage.tcmsId === vessel.tcmsId);
            return VesselUtil.formatVesselPassagesInfo(vessel, vesselRoute, vesselNextPassage);
        })

);
