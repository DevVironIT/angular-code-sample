import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@shared/shared.module';
import { OverviewPageRoutingModule } from './overview-page-routing.module';
import { OverviewPageComponent } from './components/overview-page/overview-page.component';
import { OverviewPageOverlayCollection } from './services/overview-page.overlay-collection';
import { reducers } from './store';
import { VesselsModule } from '@vessels/vessels.module';
import { IssuesModule } from '@issues/issues.module';
import { OverviewPageEffects } from './store/effects/overview-page.effects';
import { ZonesModule } from '@zones/zones.module';
import { WidgetModule } from '@widget/widget.module';
import { GeotiffModule } from '@geotiff/geotiff.module';
import { AisModule } from '@ais/ais.module';
import { PopupModule } from '@popup/popup.module';
import { PopupComponent } from '@popup/components';
import { WeatherModule } from '@weather/weather.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        WidgetModule,
        VesselsModule,
        IssuesModule,
        ZonesModule,
        GeotiffModule,
        WeatherModule,
        AisModule,
        PopupModule,
        OverviewPageRoutingModule,
        StoreModule.forFeature('overview-page', reducers),
        EffectsModule.forFeature([OverviewPageEffects]),
        NgbModule
    ],
    declarations: [OverviewPageComponent],
    exports: [OverviewPageComponent],
    entryComponents: [PopupComponent],
    providers: [OverviewPageOverlayCollection]
})
export class OverviewPageModule { }
