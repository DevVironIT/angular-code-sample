import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { of } from 'rxjs';

import { SharedModule } from '@shared/shared.module';
import { VesselsModule } from '@vessels/vessels.module';
import { ZonesModule } from '@zones/zones.module';
import { OverviewPageComponent } from './overview-page.component';
import { OverviewPageOverlayCollection } from '@pages/overview/services/overview-page.overlay-collection';
import { OverlayCollection } from '@shared/map/models/overlay-collection';
import { reducers, State } from '@pages/overview/store';
import * as fromZones from '@zones/store';
import * as fromVoyages from '@voyages/store';
import * as fromVessels from '@vessels/store';
import * as fromGeotiff from '@geotiff/store';
import * as fromWidgets from '@widget/store';
import * as fromRoutes from '@routes/store';
import * as fromPopupStore from '@popup/store';
import * as fromProfile from '@profile/store';
import * as fromRoot from '@store/index';
import * as fromLayerManager from '@layer-manager/store';
import * as fromAuth from '@auth/store';
import { CoreModule } from '@core/core.module';
import { GeotiffModule } from '@geotiff/geotiff.module';
import { WidgetModule } from '@widget/widget.module';
import { IssuesModule } from '@issues/issues.module';
import { IssueHubService } from '@issues/services';
import { FleetPageModule } from '@pages/fleet/fleet-page.module';
import { MapFeatureInjectionToken } from '@shared/map/models/map-feature-injection-token.model';

describe('OverviewComponent', () => {
    let component: OverviewPageComponent;
    let fixture: ComponentFixture<OverviewPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [OverviewPageComponent],
            imports: [
                HttpClientModule,
                StoreModule.forRoot(fromRoot.reducers, {}),
                StoreModule.forFeature('overview-page', reducers),
                StoreModule.forFeature('zones', fromZones.reducers),
                StoreModule.forFeature('voyages', fromVoyages.reducers),
                StoreModule.forFeature('vessels', fromVessels.reducers),
                StoreModule.forFeature('profiles', fromProfile.reducers),
                StoreModule.forFeature('geotiff', fromGeotiff.reducers),
                StoreModule.forFeature('widgets', fromWidgets.reducers),
                StoreModule.forFeature('routes', fromRoutes.reducers),
                StoreModule.forFeature('popups', fromPopupStore.reducers),
                StoreModule.forFeature('layerManager', fromLayerManager.reducers),
                StoreModule.forFeature('auth', fromAuth.reducers),
                EffectsModule.forRoot([]),
                CoreModule,
                SharedModule,
                VesselsModule,
                IssuesModule,
                ZonesModule,
                GeotiffModule,
                WidgetModule,
                FleetPageModule
            ],
            providers: [
                { provide: OverviewPageOverlayCollection, useValue: new OverlayCollection(<Store<State>>{}) },
                { provide: IssueHubService, useValue: {} },
                { provide: MapFeatureInjectionToken, useValue: { selectedVesselId$: of(null) }}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OverviewPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
