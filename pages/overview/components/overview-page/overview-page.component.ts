import { Component, ViewChild, OnInit, OnDestroy, Injector } from '@angular/core';
import { select, Store } from '@ngrx/store';
import 'golden-layout';
import * as GoldenLayout from 'golden-layout';
import { Feature } from 'geojson';
import { Observable, of, combineLatest, Subject } from 'rxjs';
import { map, take, takeUntil, distinctUntilChanged, filter } from 'rxjs/operators';
import { Map, LatLngTuple, LatLngBounds, latLngBounds, latLng } from 'leaflet';
import { Router } from '@angular/router';

import * as fromStore from '@store/index';
import * as fromVessels from '@vessels/store';
import * as fromZones from '@zones/store';
import * as fromPopup from '@popup/store';
import * as fromOverview from '@pages/overview/store';
import * as fromVoyages from '@voyages/store';
import * as fromProfile from '@profile/store';
import * as fromWidgets from '@widget/store';
import * as fromLayerManager from '@layer-manager/store';
import { Vessel, Kpi, VesselFeatureCode } from '@vessels/models';
import { OverviewPageOverlayCollection } from '@pages/overview/services';
import {
    MapToolOptions,
    ChartState,
    MapWidgetsOptions,
    ZoneState,
    MapButtonOptions,
    PortState,
    NavWarningZoneState,
    MapFeatureInjectionToken,
    GeoJsonFeature
} from '@shared/map/models';
import { Dictionary, Error, HamburgerMenuItem } from '@shared/models';
import {
    ZoneGroup,
    ZoneGeometry,
    ZoneProperties,
    ZoneIssueRule,
    ZoneEditPhase
} from '@zones/models';
import { VesselPassagesInfo } from '@voyages/models';
import { AppActionScheduler } from '@core/services';
import { WidgetsList } from '@widget/models';
import { WidgetContainerComponent } from '@widget/components';
import { UaisOverviewState } from '@ais/models';
import { WeatherMapFeature, WeatherLegendControlOptions } from '@weather/models';
import { GeotiffMapFeature } from '@geotiff/models';
import { PopupPrefix } from '@popup/constatnts';
import { Pages } from '@shared/constants';
import { LayerManagerSettings } from '@layer-manager/models';
import { VesselMapFeature } from '@vessels/models/vessel-map-feature.model';
import { PopupConfig } from '@popup/models';
import { VesselScheduleDialogComponent } from '@vessels/components';
@Component({
    selector: 'app-overview-page',
    templateUrl: './overview-page.component.html',
    styleUrls: ['./overview-page.component.scss']
})
export class OverviewPageComponent implements OnInit, OnDestroy {
    @ViewChild(WidgetContainerComponent, { static: false })
    widgetContainer: WidgetContainerComponent;
    allVessels$: Observable<Vessel[]>;
    vessels$: Observable<Vessel[]>;
    summaryKpi$: Observable<Kpi>;
    selectedVesselId$: Observable<number>;
    mapCenter$: Observable<LatLngTuple>;
    mapBounds$: Observable<LatLngBounds>;
    measureOptions$: Observable<MapToolOptions>;
    mapWidgetsOptions$: Observable<MapWidgetsOptions>;
    zoneGroups$: Observable<Dictionary<ZoneGroup>>;
    selectedZone$: Observable<Feature<ZoneGeometry, ZoneProperties>>;
    layerManagerButtonOptions$: Observable<MapButtonOptions>;
    isLayerManagerVisible$: Observable<boolean>;
    zoneIssueRules$: Observable<ZoneIssueRule[]>;
    zoneIssueRulesError$: Observable<Error>;
    passages$: Observable<VesselPassagesInfo[]>;
    selectedVesselTcmsId$: Observable<string>;
    goldenLayoutConfig$: Observable<GoldenLayout.Config>;
    userZones$: Observable<GeoJsonFeature[]>;
    widgets: Dictionary<any>;
    pages = Pages;
    hasCellViewFeature$: Observable<boolean>;
    hamburgerMenuConfig: HamburgerMenuItem[];
    layerManagerSettings: LayerManagerSettings;
    mapFeatureInjector: Injector;

    private destroyed$ = new Subject();
    private loadPassageInfoSchedulerActionId: string;

    constructor(
        private store: Store<fromStore.State>,
        private appActionScheduler: AppActionScheduler,
        public overlayCollection: OverviewPageOverlayCollection,
        private router: Router,
        private injector: Injector
    ) { }

    ngOnInit() {
        this.allVessels$ = this.store.pipe(select(fromVessels.getAllVessels));
        this.vessels$ = this.allVessels$
            .pipe(
                map(vessels => vessels.filter(this.hasCriticalOrHighIssue))
            )

        this.summaryKpi$ = this.vessels$
            .pipe(
                map(vessels => {
                    const summaryKpi = new Kpi();
                    vessels.forEach(vessel => {
                        summaryKpi.add(vessel.issueInfo.kpi);
                    });
                    return summaryKpi;
                })
            );
        
        // added loading all notifications because of missing VesselGuard. Notifications starts loading before vessels does
        this.allVessels$
            .pipe(
                takeUntil(this.destroyed$),
                filter(vessels => !!vessels.length),
                take(1)
            )
            .subscribe(
                () => {
                    this.store.dispatch(new fromOverview.LoadVesselsPassageInfo()); // this action triggers loading vessels notifications 
                }
            )

        this.selectedVesselId$ = this.store.pipe(select(fromOverview.getSelectedVesselId));
        this.mapCenter$ = this.store.pipe(select(fromOverview.getMapCenter));
        this.mapBounds$ = this.store.pipe(select(fromOverview.getMapBounds));
        this.zoneGroups$ = this.store.pipe(select(fromZones.getZoneGroupsEntities));
        this.selectedZone$ = this.store.pipe(select(fromZones.getSelectedZoneInfo));
        this.isLayerManagerVisible$ = this.store.pipe(select(fromOverview.getLayerManagerState));
        this.zoneIssueRules$ = this.store.pipe(select(fromZones.getSelectedZoneIssueRules));
        this.zoneIssueRulesError$ = this.store.pipe(select(fromZones.getZoneIssueRulesError));
        this.passages$ = this.store.pipe(select(fromOverview.getVesselsPassagesInfo));
        this.selectedVesselTcmsId$ = this.store.pipe(select(fromOverview.getSelectedVesselTcmsId));
        this.goldenLayoutConfig$ = this.store.select(fromWidgets.getWidgetsSettings, Pages.overviewPage);
        this.userZones$ = this.store.pipe(select(fromZones.getUserZones));
        this.hasCellViewFeature$ = this.store.pipe(select(fromVessels.hasFeature(VesselFeatureCode.cellView)));

        combineLatest(
            this.selectedVesselId$,
            this.store.pipe(select(fromVessels.getAllVessels))
        ).pipe(
            take(1)
        ).subscribe(([selectedVesselId, vessels]) => {
            if (selectedVesselId !== null || !vessels.length) {
                return;
            } else if (vessels.length === 1) {
                this.store.dispatch(new fromOverview.CenterMapOnVessel(vessels[0]));
            } else {
                const bounds = this.boundsForVessels(vessels);
                if (bounds.isValid()) {
                    this.store.dispatch(new fromOverview.SetMapBounds(bounds));
                }
            }
        });

        this.store.select(fromLayerManager.getPageSettings)
            .pipe(
                takeUntil(this.destroyed$)
            )
            .subscribe(settings => {
                this.layerManagerSettings = settings;
            });

        this.store.dispatch(new fromVessels.LoadAllVesselsInfo());

        this.loadPassageInfoSchedulerActionId = this.appActionScheduler.add({
            actionType: fromVoyages.VesselPassagesInfoActionTypes.LoadVesselPassagesInfos,
            periodInSec: 60
        });

        this.mapFeatureInjector = Injector.create({
            providers: [
                {
                    provide: MapFeatureInjectionToken,
                    deps: [],
                    useValue: { selectedVesselId$: this.selectedVesselId$ }
                }
            ],
            parent: this.injector
        });

        this.widgets = {
            [WidgetsList.kpi.componentName]: {
                kpi$: this.summaryKpi$
            },
            [WidgetsList.map.componentName]: {
                qaId: 'overview-page-map',
                overlayCollection: this.overlayCollection,
                chartState$: this.store.select(fromLayerManager.getChartState),
                zoneState$: this.store.select(fromLayerManager.getZoneState),
                portState$: this.store.select(fromLayerManager.getPortState),
                uaisState$: this.store.select(fromLayerManager.getUaisState),
                userZoneState$: this.store.select(fromLayerManager.getUserZoneState),
                navWarningZoneState$: this.store.select(fromLayerManager.getNavWarningZoneState),
                mapCenter$: this.mapCenter$,
                mapBounds$: this.mapBounds$,
                isLayerManagerVisible$: this.isLayerManagerVisible$,
                zoneGroups$: this.zoneGroups$,
                measureOptions$: this.measureOptions$,
                mapWidgetsOptions$: this.mapWidgetsOptions$,
                selectedZone$: this.selectedZone$,
                zoneIssueRules$: this.zoneIssueRules$,
                zoneIssueRulesError$: this.zoneIssueRulesError$,
                layerManagerButtonOptions$: this.layerManagerButtonOptions$,
                userZones$: this.userZones$,
                userZone$: this.store.pipe(select(fromZones.getZone)),
                isMVTAvailable$: this.store.pipe(select(fromProfile.getOrganizationMvtAccess)),
                isUserZonePopupVisible$: this.store.pipe(select(fromZones.isEditingGeometryPhase)),
                isUserZoneInfoVisible$: this.store.pipe(select(fromZones.isEditingInfoPhase)),
                isUserZoneByCoordsVisible$: this.store.pipe(select(fromZones.isCreatingInfoPhaseByCoords)),
                isUserZoneSaving$: this.store.pipe(select(fromZones.isUserZoneSaving)),
                zoneAlarms$: this.store.select(fromZones.getUserZoneAlarm),
                isAlarmsSaving$: this.store.select(fromZones.isAlarmsSaving),
                isConfirmationPopupVisible$: this.store.pipe(select(fromOverview.getConfirmationPopupVisibility)),
                selectedNavWarningZone$: this.store.pipe(select(fromZones.getSelectedNavWarningZoneInfo)),
                mapReady: (_map: Map) => this.mapReady(_map),
                portStateChanged: (portState: PortState) => this.onPortStateChanged(portState),
                closeLayerManager: () => this.onCloseLayerManager(),
                toggleLayerManagerState: () => this.toggleLayerManagerState(),
                onCloseLayerManager: () => this.onCloseLayerManager(),
                onChartStateChanged: (chartState: ChartState) => this.onChartStateChanged(chartState),
                onZoneStateChanged: (zoneState: ZoneState) => this.onZoneStateChanged(zoneState),
                onPortStateChanged: (portState: PortState) => this.onPortStateChanged(portState),
                onDeselectZoneInfo: () => this.onDeselectZoneInfo(),
                enableZoneDrawer: (zoneType: string) => this.enableZoneDrawer(zoneType),
                createZoneByCoordinates: (zoneType: string) => this.createZoneByCoordinates(zoneType),
                onUaisStateChanged: (uaisState: UaisOverviewState) => this.onUaisStateChanged(uaisState),
                showConfirmationPopup: (data: any) => this.showConfirmationPopup(data),
                deleteZone: (zoneId: string) => this.onDeleteZone(zoneId),
                editZone: (zone: Feature<ZoneGeometry, ZoneProperties>) => this.onEditZone(zone),
                hideConfirmationDialog: () => this.hideConfirmationDialog(),
                navWarningZoneStateChanged: (state: NavWarningZoneState) => this.onNavWarningZoneStateChanged(state),
                deselectNavWarningZone: () => this.deselectNavWarningZone(),
                toggleUserZoneEnabled: () => this.toggleUserZoneEnabled(),
                mapFeatures: [
                    new GeotiffMapFeature(this.store, null, null),
                    new WeatherMapFeature(this.store, null),
                    new VesselMapFeature(this.store, null, null, null, null, null, false, null, this.mapFeatureInjector)
                ]
            },
            [WidgetsList.passages.componentName]: {
                allVessels$: this.allVessels$,
                passages$: this.passages$,
                selectedVesselTcmsId$: this.selectedVesselTcmsId$,
                schedules$: this.store.select(fromVessels.getAllVesselScheduleData),
                selectVessel: (vessel: Vessel) => this.selectVessel(vessel),
                openPopup: (data) => this.openSchedulePopup(data)
            },
            [WidgetsList.vesselList.componentName]: {
                vessels$: this.vessels$,
                selectedVesselId$: this.selectedVesselId$,
                selectVessel: (vessel: Vessel) => this.selectVessel(vessel)
            }
        };

        this.hamburgerMenuConfig = [
            {
                name: 'Manage widgets',
                click: () => { this.store.dispatch(new fromWidgets.ToggleWidgetsConstructorVisibility()); },
                qaId: 'widget-constructor'
            },
            {
                name: 'Open Cell View',
                click: () => { this.openCellView(); },
                qaId: 'open-cell-view'
            },
            {
                name: 'Listen to Cell View',
                click: () => { this.listenToCellView(); },
                qaId: 'listen-cell-view'
            }
        ];

        this.store.select<Vessel>(fromOverview.getSelectedVessel)
            .pipe(
                takeUntil(this.destroyed$),
                distinctUntilChanged(
                    (currentVessel: Vessel, newVessel: Vessel) => currentVessel && newVessel && currentVessel.id === newVessel.id
                )
            )
            .subscribe(vessel => {
                this.store.dispatch(new fromVessels.LoadOwnAisData(vessel));
            });
    }

    selectVessel(vessel: Vessel) {
        this.store.dispatch(new fromOverview.SelectVessel(vessel));
        this.store.dispatch(new fromOverview.CenterMapOnVessel(vessel));
        this.store.dispatch(new fromVessels.LoadTrack({ vesselId: vessel.id }));
        this.store.dispatch(new fromVessels.LoadVesselsRouteProgress([vessel]));
    }

    deselectVessel() {
        this.store.dispatch(new fromOverview.DeselectVessel());
    }

    onChartStateChanged(chartState: ChartState) {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            chartState: chartState
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
    }

    onZoneStateChanged(zoneState: ZoneState) {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            zoneState: zoneState
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
        this.onDeselectZoneInfo();
    }

    onPortStateChanged(portState: PortState) {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            portState: portState
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
    }

    mapReady(_map: Map) {
        this.widgets[WidgetsList.map.componentName].layerManagerButtonOptions$ = of<MapButtonOptions>({
            position: 'bottomright',
            map: _map,
            qaId: 'layer-manager-button',
            iconName: 'layer-manager'
        });
        this.widgets[WidgetsList.map.componentName].measureOptions$ = of<MapToolOptions>({
            position: 'topright',
            map: _map,
            toggleMapTool: toolId => { this.store.dispatch(new fromOverview.ToggleMapTool(toolId)); },
            activeTool: this.store.select<string>(fromOverview.getActiveMapTool)
        });
        this.widgets[WidgetsList.map.componentName].mapWidgetsOptions$ = of<MapWidgetsOptions>({
            position: 'bottomleft',
            map: _map
        });
        this.widgets[WidgetsList.map.componentName].eniramWeatherLegendControlOptions$ = of<WeatherLegendControlOptions>({
            position: 'topleft',
            map: _map
        });
    }

    hasCriticalOrHighIssue(vessel: Vessel): boolean {
        return vessel.issueInfo.issueCount.high > 0 || vessel.issueInfo.issueCount.critical > 0;
    }

    boundsForVessels(vessels: Vessel[]): LatLngBounds {
        const positions = vessels
            .filter(vessel => !!vessel.freshestPosition)
            .map(vessel => latLng(vessel.freshestPosition.lat, vessel.freshestPosition.lon));
        return latLngBounds(positions);
    }

    onDeselectZoneInfo() {
        this.store.dispatch(new fromZones.DeselectZoneInfo());
    }

    deselectNavWarningZone() {
        this.store.dispatch(new fromZones.DeselectNavWarningZoneInfo());
    }

    toggleLayerManagerState() {
        this.store.dispatch(new fromOverview.ToggleLayerManager());
    }

    onUaisStateChanged(uaisState: UaisOverviewState) {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            uaisState: uaisState
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
    }

    togglePassageWidgetVisibility() {
        this.store.dispatch(new fromOverview.TogglePassageWidget());
    }

    onCloseLayerManager() {
        this.store.dispatch(new fromOverview.CloseLayerManager());
    }

    resetWidgetsSettings() {
        this.store.dispatch(new fromWidgets.ResetSettings(Pages.overviewPage));
    }

    enableZoneDrawer(zoneType: string) {
        this.store.dispatch(new fromOverview.CloseLayerManager());
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            userZoneState: {
                ...this.layerManagerSettings.userZoneState,
                isEnabled: true
            }
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
        this.store.dispatch(new fromZones.SetPhase(ZoneEditPhase.editingGeometry));
    }

    createZoneByCoordinates(zoneType: string) {
        this.store.dispatch(new fromOverview.CloseLayerManager());
        this.store.dispatch(new fromZones.LoadUserZoneAlarm());
        this.store.dispatch(new fromZones.SetPhase(ZoneEditPhase.creatingZoneByCoordinates));
    }

    onDeleteZone(zoneId: string) {
        this.store.dispatch(new fromZones.DeleteZone(zoneId));
        this.hideConfirmationDialog();
    }

    onEditZone(zone: GeoJsonFeature) {
        this.store.dispatch(new fromOverview.CloseLayerManager());
        this.store.dispatch(new fromZones.SetZoneToEdit(zone));
        this.store.dispatch(new fromZones.LoadUserZoneAlarm());
    }

    showConfirmationPopup(data: any) {
        this.widgets[WidgetsList.map.componentName].additionalData = data;
        this.store.dispatch(new fromOverview.SetConfirmationPopupVisibility(true));
    }

    onNavWarningZoneStateChanged(state: NavWarningZoneState) {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            navWarningZoneState: state
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
        this.deselectNavWarningZone();
    }

    hideConfirmationDialog() {
        this.store.dispatch(new fromOverview.SetConfirmationPopupVisibility(false));
    }

    toggleUserZoneEnabled() {
        const settings: LayerManagerSettings = {
            ...this.layerManagerSettings,
            userZoneState: {
                ...this.layerManagerSettings.userZoneState,
                isEnabled: !this.layerManagerSettings.userZoneState.isEnabled
            }
        };
        this.store.dispatch(new fromLayerManager.UpdateSettings({ pageName: Pages.overviewPage, settings: settings }));
    }

    ngOnDestroy() {
        this.appActionScheduler.remove(this.loadPassageInfoSchedulerActionId);
        this.onDeselectZoneInfo();
        this.onCloseLayerManager();
        this.hideConfirmationDialog();
        this.deselectVessel();
        this.store.dispatch(new fromZones.Finish());
        this.store.dispatch(new fromPopup.CloseAllPopups(PopupPrefix.all));
        this.destroyed$.next();
    }

    listenToCellView() {
        const wallId = prompt('Please input video wall id.', 'wall#1');
        if (!wallId) {
            return;
        }
        this.router.navigate(['/fleet'], {
            queryParams: { 'video-wall-id': wallId }
        });
    }

    openCellView() {
        const wallId = prompt('Please input video wall id.', 'wall#1');
        if (!wallId) {
            return;
        }
        this.router.navigate(['/overview-video-wall'], {
            queryParams: { 'video-wall-id': wallId }
        });
    }

    openSchedulePopup(data: { vesselIMO: number, status: string, event: any }) {
        this.store.dispatch(new fromPopup.CloseAllPopups(PopupPrefix.all));
        const popupConfig: PopupConfig = {
            id: `${PopupPrefix.schedule}${data.vesselIMO}`,
            childComponent: VesselScheduleDialogComponent,
            getName: () => of('Cargo operations'),
            getData: () => data.status === 'current'
                ? this.store.select(fromVessels.getCurrentScheduleDataForVessel(data.vesselIMO))
                : this.store.select(fromVessels.getFutureScheduleDataForVessel(data.vesselIMO)),
            position: [data.event.pageX - 16, data.event.pageY + 16],
            classNames: ['vessel-popup']
        };
        this.store.dispatch(new fromPopup.OpenPopup(popupConfig));
    }
}
