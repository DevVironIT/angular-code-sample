import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, take, map, switchMap } from 'rxjs/operators';

import * as fromVessels from '../store';

@Injectable()
export class VesselExistsGuard implements CanActivate {

    constructor(
        private store: Store<fromVessels.State>,
        private router: Router) { }

    waitForCollectionToLoad(): Observable<boolean> {
        return this.store
            .select(fromVessels.getVesselsLoaded)
            .pipe(
                filter(loaded => loaded),
                take(1)
            );
    }

    hasVessel(id: string): Observable<boolean> {
        return this.store
            .select(fromVessels.getVesselEntitiesState)
            .pipe(
                map(vessels => {
                    if (vessels.entities[id]) {
                        return true;
                    }
                    this.router.navigate(['/404']);
                    return false;
                })
            );
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        return this.waitForCollectionToLoad().pipe(
            switchMap(() => this.hasVessel(route.params['vesselId']))
        );
    }

}
