import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

import * as fromVessels from '../store';

@Injectable()
export class VesselGuard implements CanActivate {

    constructor(private store: Store<fromVessels.State>) { }

    canActivate(): Observable<boolean> {
        return this.store
            .select(fromVessels.getVesselsLoaded)
            .pipe(
                filter(loaded => loaded),
                take(1)
            );
    }
}
