import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselStatusComponent } from './vessel-status.component';
import { generateVessel } from '../../../testing/factories';

describe('VesselStatusComponent', () => {
    let component: VesselStatusComponent;
    let fixture: ComponentFixture<VesselStatusComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VesselStatusComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VesselStatusComponent);
        component = fixture.componentInstance;
        component.vessel = generateVessel();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
