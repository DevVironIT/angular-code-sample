import { Component, Input } from '@angular/core';

import { Vessel } from '../../models';

@Component({
    selector: 'vessel-status',
    templateUrl: './vessel-status.component.html',
    styleUrls: ['./vessel-status.component.scss']
})
export class VesselStatusComponent {

    @Input() vessel: Vessel;

    constructor() { }

}
