import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Observable ,  Subscription } from 'rxjs';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

import { Vessel } from '../../models/vessel.model';

@Component({
    selector: 'vessel-list',
    templateUrl: './vessel-list.component.html',
    styleUrls: ['./vessel-list.component.scss']
})
export class VesselListComponent implements OnInit, OnDestroy {
    vesselsSubscription: Subscription;

    @Input() selectedVesselId$: Observable<string>;
    @Input() vesselCollection$: Observable<Vessel[]>;

    @Output() vesselClick = new EventEmitter<Vessel>();

    @ViewChild(PerfectScrollbarDirective, { static: false }) scroll: PerfectScrollbarDirective;

    constructor() { }

    ngOnInit() {
        this.vesselsSubscription = this.vesselCollection$.subscribe(() => { this.scroll?.update(); });
    }

    ngOnDestroy() {
        this.vesselsSubscription.unsubscribe();
    }

    selectVessel(vessel: Vessel) {
        this.vesselClick.emit(vessel);
    }
}
