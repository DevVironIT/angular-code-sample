import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { of } from 'rxjs';

import { SharedModule } from '../../../shared/shared.module';
import { VesselListComponent } from './vessel-list.component';
import { Vessel } from '../../models';

describe('VesselListComponent', () => {
    let component: VesselListComponent;
    let fixture: ComponentFixture<VesselListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                PerfectScrollbarModule,
                StoreModule.forRoot({}),
                EffectsModule.forRoot([])
            ],
            declarations: [VesselListComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VesselListComponent);
        component = fixture.componentInstance;
        component.vesselCollection$ = of<Vessel[]>([]);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
