import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { of } from 'rxjs';

import * as fromRoot from '@store/index';
import { SharedModule } from '@shared/shared.module';
import { VesselDetailsComponent } from './vessel-details.component';
import { VesselStatusComponent } from '../vessel-status/vessel-status.component';
import { generateVessel } from '@testing/factories';
import { VoyagePortsInfoComponent } from '../voyage-ports-info/voyage-ports-info.component';
import { CoreModule } from '@core/core.module';
import { VesselPositionLastUpdateComponent } from '../vessel-position-last-update/vessel-position-last-update.component';

describe('VesselDetailsComponent', () => {
    let component: VesselDetailsComponent;
    let fixture: ComponentFixture<VesselDetailsComponent>;
    let compiledElement: HTMLElement;
    const vessel = generateVessel();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                CoreModule,
                NgbModule,
                RouterTestingModule,
                ReactiveFormsModule,
                StoreModule.forRoot(fromRoot.reducers, {}),
                EffectsModule.forRoot([])
            ],
            declarations: [
                VesselDetailsComponent,
                VesselStatusComponent,
                VoyagePortsInfoComponent,
                VesselPositionLastUpdateComponent
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VesselDetailsComponent);
        component = fixture.componentInstance;
        component.selectedVessel$ = of(vessel);
        fixture.detectChanges();
        compiledElement = fixture.debugElement.nativeElement;
    });

    it('should create', () => {
       expect(component).toBeTruthy();
    });

    it('should add url to a href', () => {
        expect(compiledElement.querySelector('a').textContent).toContain('Real Time Monitoring');
        expect(compiledElement.querySelector('a').href).toContain('/rtm/' + vessel.tcmsId);
    });

    it('should hide rtm button, if vessel does not have rights', () => {
        vessel.hasRtmFeature = false;
        component.selectedVessel$ = of(vessel);
        fixture.detectChanges();
        expect(compiledElement.querySelector('footer').textContent).not.toContain('Real Time Monitoring');
    });

});
