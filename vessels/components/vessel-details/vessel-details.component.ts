import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Vessel, VoyagePortsEditorData } from '@vessels/models';

@Component({
    selector: 'vessel-details',
    templateUrl: './vessel-details.component.html',
    styleUrls: ['./vessel-details.component.scss']
})
export class VesselDetailsComponent {

    @Input() selectedVessel$: Observable<Vessel>;

    @Output() savePortsInfo = new EventEmitter<VoyagePortsEditorData>();

    constructor() { }

    onSaveClick(ports) {
        this.savePortsInfo.emit(ports);
    }

}
