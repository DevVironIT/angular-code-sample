import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as moment from 'moment';

import * as fromVessel from './vessel.reducer';
import * as fromVesselIssueInfo from './vessel-issue-info.reducer';
import * as fromVesselConnectivityStatus from './vessel-connectivity-status.reducer';
import * as fromVesselGroup from './vessel-group.reducer';
import * as fromVesselFeatures from './vessel-features.reducer';
import * as fromVesselInfo from './vessel-info.reducer';
import * as fromVesselFilter from './vessel-filter.reducer';
import * as fromOwnAisData from './own-ais-data.reducer';
import * as fromVesselTrack from './vessel-track.reducer';
import * as fromVesselTelemetry from './vessel-telemetry.reducer';
import * as fromVesselCloudEta from './vessel-cloud-eta.reducer';
import * as fromVesselNextPassage from './vessel-next-passage.reducer';
import * as fromVesselEcdisData from './vessel-ecdis-data.reducer';
import * as fromVesselRouteProgress from './vessel-route-progress.reducer';
import * as fromVesselSchedule from './vessel-schedule.reducer';
import * as fromRoot from '@store/reducers';
import { IssueCounts, Vessel, VesselIssueInfo, VesselFeatureCode, VesselTrack, VesselTelemetry, VesselCloudEta, VesselScheduleData } from '@vessels/models';
import { Dictionary } from '@shared/models';
import { VesselUtil } from '@vessels/utils';

export interface VesselState {
    vessels: fromVessel.State;
    vesselGroups: fromVesselGroup.State;
    vesselFeatures: fromVesselFeatures.State;
    vesselInfos: fromVesselInfo.State;
    vesselFilter: fromVesselFilter.State;
    issueInfos: fromVesselIssueInfo.State;
    vesselsConnectivityStatus: fromVesselConnectivityStatus.State;
    ownAisData: fromOwnAisData.State;
    vesselTracks: fromVesselTrack.State;
    vesselRouteTelemetry: fromVesselTelemetry.State;
    vesselCloudEta: fromVesselCloudEta.State;
    vesselNextPassage: fromVesselNextPassage.State;
    vesselEcdisData: fromVesselEcdisData.State;
    vesselRouteProgress: fromVesselRouteProgress.State;
    vesselSchedule: fromVesselSchedule.State;
}

export interface State extends fromRoot.State {
    vessels: VesselState;
}

export const reducers = {
    vessels: fromVessel.reducer,
    vesselGroups: fromVesselGroup.reducer,
    vesselFeatures: fromVesselFeatures.reducer,
    vesselInfos: fromVesselInfo.reducer,
    vesselFilter: fromVesselFilter.reducer,
    issueInfos: fromVesselIssueInfo.reducer,
    vesselsConnectivityStatus: fromVesselConnectivityStatus.reducer,
    ownAisData: fromOwnAisData.reducer,
    vesselTracks: fromVesselTrack.reducer,
    vesselRouteTelemetry: fromVesselTelemetry.reducer,
    vesselCloudEta: fromVesselCloudEta.reducer,
    vesselNextPassage: fromVesselNextPassage.reducer,
    vesselEcdisData: fromVesselEcdisData.reducer,
    vesselRouteProgress: fromVesselRouteProgress.reducer,
    vesselSchedule: fromVesselSchedule.reducer
};

export const getVesselsState = createFeatureSelector<VesselState>('vessels');

export const getVesselEntitiesState = createSelector(getVesselsState, state => state.vessels);
export const { selectEntities: getVesselEntities } = fromVessel.adapter.getSelectors(getVesselEntitiesState);
export const getVesselsLoaded = createSelector(getVesselsState, state => state.vessels.isLoaded);
export const isVesselsLoadingError = createSelector(getVesselsState, state => !!state.vessels.loadingError);
export const getVesselsLoading = createSelector(getVesselsState, state => state.vessels.isLoading);

export const getVesselGroupEntitiesState = createSelector(getVesselsState, state => state.vesselGroups);
export const { selectAll: getAllGroups } = fromVesselGroup.adapter.getSelectors(getVesselGroupEntitiesState);

export const getVesselInfoEntitiesState = createSelector(getVesselsState, state => state.vesselInfos);
export const { selectEntities: getVesselInfoEntities } = fromVesselInfo.adapter.getSelectors(getVesselInfoEntitiesState);
export const { selectAll: getAllVesselsInfo } = fromVesselInfo.adapter.getSelectors(getVesselInfoEntitiesState);
export const getAllVesselsInfoLoaded = createSelector(getVesselsState, state => state.vesselInfos.isAllLoaded);
export const getAllVesselsInfoLoading = createSelector(getVesselsState, state => state.vesselInfos.isAllLoading);
export const getAllVesselsInfoFailed = createSelector(getVesselsState, state => state.vesselInfos.isAllFailed);
export const getVesselInfoSaveStatus = createSelector(getVesselsState, state => state.vesselInfos.saveStatus);

export const getIssueInfoState = createSelector(getVesselsState, state => state.issueInfos);
export const { selectEntities: getIssueInfoEntities } = fromVesselIssueInfo.adapter.getSelectors(getIssueInfoState);

export const getVesselConnectivityStatusState = createSelector(getVesselsState, state => state.vesselsConnectivityStatus);
export const getDisconnectedVesselsDictionary = createSelector(
    getVesselConnectivityStatusState, state => state.disconnectedVesselsDictionary
);

export const getVesselFeaturesEntitiesState = createSelector(getVesselsState, state => state.vesselFeatures);
export const getVesselFeaturesEntitiesLoaded = createSelector(getVesselFeaturesEntitiesState, state => state.isLoaded);
export const { selectEntities: getAllFeatures } = fromVesselFeatures.adapter.getSelectors(getVesselFeaturesEntitiesState);
export const getFeaturesLoaded = createSelector(getVesselFeaturesEntitiesState, state => state.isLoaded);

export const getOwnAisDataEntitiesState = createSelector(getVesselsState, state => state.ownAisData);
export const { selectEntities: getOwnAisDataEntities } = fromOwnAisData.adapter.getSelectors(getOwnAisDataEntitiesState);
export const { selectAll: getAllOwnAisData } = fromOwnAisData.adapter.getSelectors(getOwnAisDataEntitiesState);

export const getVesselFilterQuery = createSelector(getVesselsState, state => state.vesselFilter.query);

export const getVesselsTrackEntitiesState = createSelector(getVesselsState, state => state.vesselTracks);
export const { selectAll: getVesselsTracks } = fromVesselTrack.adapter.getSelectors(getVesselsTrackEntitiesState);
export const getSelectedTrackPoint = createSelector(getVesselsState, state => state.vesselTracks.selectedPoint);

export const getVesselRouteTelemetryState = createSelector(getVesselsState, state => state.vesselRouteTelemetry);
export const { selectAll: getAllVesselRouteTelemetry } = fromVesselTelemetry.adapter.getSelectors(getVesselRouteTelemetryState);

export const getVesselCloudEtaState = createSelector(getVesselsState, state => state.vesselCloudEta);
export const { selectAll: getAllVesselCloudEta } = fromVesselCloudEta.adapter.getSelectors(getVesselCloudEtaState);

export const getVesselNextPassageState = createSelector(getVesselsState, state => state.vesselNextPassage);
export const { selectAll: getAllVesselNextPassages } = fromVesselNextPassage.adapter.getSelectors(getVesselNextPassageState);

export const getVesselEcdisDataState = createSelector(getVesselsState, state => state.vesselEcdisData);
export const { selectAll: getAllVesselEcdisData } = fromVesselEcdisData.adapter.getSelectors(getVesselEcdisDataState);

export const getVesselRouteProgressState = createSelector(getVesselsState, state => state.vesselRouteProgress);
export const { selectAll: getAllVesselsRouteProgress } = fromVesselRouteProgress.adapter.getSelectors(getVesselRouteProgressState);

export const getVesselScheduleState = createSelector(getVesselsState, state => state.vesselSchedule);
export const { selectAll: getAllVesselScheduleData } = fromVesselSchedule.adapter.getSelectors(getVesselScheduleState);

export function hasFeature(feature: string) {
    return createSelector(
        getAllFeatures,
        (vesselFeaturesEntities) => Object
            .values(vesselFeaturesEntities)
            .some(features => features.features.some(f => f.code === feature))
    );
}

export const getVesselDictionaryByDmsClientId = createSelector(
    getVesselEntities,
    (vesselEntities) => {
        const dictionary: Dictionary<Vessel> = {};
        Object
            .values(vesselEntities)
            .forEach(vessel => { dictionary[vessel.dmsClientId] = vessel; });
        return dictionary;
    }
);

export const getAllVessels = createSelector(
    getVesselEntities,
    getAllFeatures,
    getIssueInfoEntities,
    getDisconnectedVesselsDictionary,
    getAllVesselsRouteProgress,
    getAllVesselEcdisData,
    getAllVesselCloudEta,
    (
        vesselEntities, featureEntities, issueInfoEntities,
        disconnectedVessels, routeProgress, vesselsEcdisData, cloudEtas
    ) => Object.values(vesselEntities)
        .map(vessel => {

            const hasRtmFeature = featureEntities[vessel.dmsClientId]
                && featureEntities[vessel.dmsClientId].features.some(f => f.code === VesselFeatureCode.rtm);
            const hasVoyagePlanningFeature = featureEntities[vessel.dmsClientId]
                && featureEntities[vessel.dmsClientId].features.some(f => f.code === VesselFeatureCode.voyagePlanning);
            const hasFuelFeature = featureEntities[vessel.dmsClientId]
                && featureEntities[vessel.dmsClientId].features.some(f => f.code === VesselFeatureCode.fuel);
            const progress = routeProgress?.find(progress => progress.tcmsId === vessel.tcmsId) || null;
            const eta = cloudEtas?.find(cloudEta => cloudEta.tcmsId === vessel.tcmsId) || null;
            const ecdisData = vesselsEcdisData?.find(data => data.tcmsId === vessel.tcmsId) || null;

            return {
                ...vessel,
                issueInfo: issueInfoEntities[vessel.tcmsId] || new VesselIssueInfo(vessel.tcmsId),
                voyageInfo: vessel.voyageInfo ? {
                    ...vessel.voyageInfo,
                    distanceInMeters: progress?.distanceInMeters,
                    remainingDistanceInMeters: progress?.remainingDistanceInMeters,
                    coveredDistancePercentage: progress?.coveredDistancePercentage,
                    cloudEta: eta,
                    ecdisData: ecdisData
                } : null,
                isOnline: !disconnectedVessels[vessel.tcmsId],
                hasRtmFeature: hasRtmFeature,
                hasVoyagePlanningFeature: hasVoyagePlanningFeature,
                hasFuelFeature: hasFuelFeature
            };
        })
        .sort(sortByIssuePriority)
);

export const getVideoWallVesselByTcmsId = createSelector(
    getAllVessels,
    (vessels: Vessel[], tcmsId) => vessels.find(v => v.tcmsId === tcmsId)
);

export const getVesselTrackById = createSelector(
    getVesselsTracks,
    (tracks: VesselTrack[], id) => tracks.find(t => t.vesselId === id)
);

export const getAllVesselImos = createSelector(
    getAllVessels,
    vessels => vessels.reduce((dict, vessel) => {
        dict[vessel.imo] = true;
        return dict;
    }, {})
);
export const getFilteredVessels = createSelector(
    getAllVessels,
    getVesselFilterQuery,
    (vessels, query) => {
        const queryInUppercase = query.toUpperCase();
        return vessels.filter(vessel =>
            vessel.name.toUpperCase().includes(queryInUppercase)
            || String(vessel.imo).toUpperCase().includes(queryInUppercase)
        );
    }
);

export const getVesselTelemetryByTcmsId = createSelector(
    getAllVesselRouteTelemetry,
    getAllVessels,
    (telemetry: VesselTelemetry[], vessels: Vessel[], tcmsId: string) => {
        const vessel = vessels.find(v => v.tcmsId === tcmsId);
        return vessel && vessel.isOnline ? telemetry.find(singleTelemetry => singleTelemetry.tcmsId === vessel.tcmsId) : null;
    }
);

export const getVesselCloudEtaByTcmsId = createSelector(
    getAllVesselCloudEta,
    getAllVessels,
    (cloudEtas: VesselCloudEta[], vessels: Vessel[], tcmsId: string) => {
        const vessel = vessels.find(v => v.tcmsId === tcmsId);
        return vessel && vessel.isOnline ? cloudEtas.find(cloudEta => cloudEta.tcmsId === vessel.tcmsId) : null;
    }
);


export const getVesselInfoByTcmsId = createSelector(
    getAllVesselsInfo,
    (info, tcmsId) => info.find(i => i.tcmsId === tcmsId)
);

export const getVesselByDmsClientId = createSelector(
    getAllVessels,
    (vessels, dmsClientId) => vessels.find(v => v.dmsClientId === dmsClientId)
);

export const getVesselByTcmsId = createSelector(
    getAllVessels,
    (vessels, tcmsId) => vessels.find(v => v.tcmsId === tcmsId)
);

export function getCurrentScheduleDataForVessel(imo: number) {
    return createSelector(
        getAllVesselScheduleData,
        (schedules) =>
            schedules.find(schedule =>
                Number(schedule.vesselNumberIMO) === imo && schedule.tsBeginLoad < moment().unix() && schedule.tsEndDisc > moment().unix())
    );
}

export function getFutureScheduleDataForVessel(imo: number) {
    return createSelector(
        getAllVesselScheduleData,
        (schedules) =>
            schedules
                .filter(schedule => Number(schedule.vesselNumberIMO) === imo && schedule.tsBeginLoad > moment().unix())
                .sort((a, b) => a.tsBeginLoad - b.tsBeginLoad)[0]
    );
}

const emptyIssueCount = new IssueCounts();

function sortByIssuePriority(vessel1: Vessel, vessel2: Vessel): number {
    const vesselIssueCount1 = vessel1.issueInfo ? vessel1.issueInfo.issueCount || emptyIssueCount : emptyIssueCount;
    const vesselIssueCount2 = vessel2.issueInfo ? vessel2.issueInfo.issueCount || emptyIssueCount : emptyIssueCount;

    if (vesselIssueCount1.critical < vesselIssueCount2.critical) {
        return 1;
    } else if (vesselIssueCount1.critical > vesselIssueCount2.critical) {
        return -1;
    }

    if (vesselIssueCount1.high < vesselIssueCount2.high) {
        return 1;
    } else if (vesselIssueCount1.high > vesselIssueCount2.high) {
        return -1;
    }

    if (vesselIssueCount1.info < vesselIssueCount2.info) {
        return 1;
    } else if (vesselIssueCount1.info > vesselIssueCount2.info) {
        return -1;
    }

    return 0;
}
