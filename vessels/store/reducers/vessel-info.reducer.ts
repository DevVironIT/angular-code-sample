import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

import { RequestStatuses } from '@store/index';
import { VesselInfo } from '../../models';
import { VesselInfoActions, VesselInfoActionTypes } from '../actions';

export interface State extends EntityState<VesselInfo> {
    saveStatus: RequestStatuses;
    isAllLoaded: boolean;
    isAllLoading: boolean;
    isAllFailed: boolean;
}

export const adapter: EntityAdapter<VesselInfo> = createEntityAdapter<VesselInfo>({
    selectId: (vesselInfo: VesselInfo) => vesselInfo.tcmsId
});

export const initialState: State = adapter.getInitialState({
    saveStatus: RequestStatuses.completed,
    isAllLoaded: false,
    isAllLoading: false,
    isAllFailed: false
});

export function reducer(state = initialState, action: VesselInfoActions): State {
    switch (action.type) {
        case VesselInfoActionTypes.LoadSuccess: {
            return {
                ...adapter.upsertOne(action.payload, state),
                saveStatus: RequestStatuses.completed,
            };
        }
        case VesselInfoActionTypes.LoadFailure: {
            return state;
        }
        case VesselInfoActionTypes.Save: {
            return {
                ...state,
                saveStatus: RequestStatuses.inProgress
            };
        }
        case VesselInfoActionTypes.SaveSuccess: {
            return {
                ...adapter.upsertOne(action.payload, state),
                saveStatus: RequestStatuses.completed
            };
        }
        case VesselInfoActionTypes.SaveFailure: {
            return {
                ...state,
                saveStatus: RequestStatuses.failed
            };
        }
        case VesselInfoActionTypes.LoadAll: {
            return {
                ...state,
                isAllLoading: true,
                isAllFailed: false
            };
        }
        case VesselInfoActionTypes.LoadAllSuccess: {
            return {
                ...adapter.upsertMany(action.payload, state),
                isAllLoaded: true,
                isAllLoading: false
            };
        }
        case VesselInfoActionTypes.LoadAllFailure: {
            return {
                ...state,
                isAllLoaded: false,
                isAllLoading: false,
                isAllFailed: true
            };
        }
        default: {
            return state;
        }
    }
}
