import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';

import { VesselActions, VesselActionTypes } from '@vessels/store/actions';
import { Vessel, Position, VoyageInfo } from '@vessels/models';

export interface State extends EntityState<Vessel> {
    isLoaded: boolean;
    isLoading: boolean;
    loadingError: HttpErrorResponse;
}

export const adapter: EntityAdapter<Vessel> = createEntityAdapter<Vessel>({});

export const initialState: State = adapter.getInitialState({
    isLoaded: false,
    isLoading: false,
    loadingError: null
});

export function reducer(
    state = initialState,
    action: VesselActions
): State {
    switch (action.type) {
        case VesselActionTypes.LoadingStarted: {
            return {
                ...state,
                isLoading: true
            };
        }
        case VesselActionTypes.LoadFailure: {
            // TODO: notify user?
            return {
                ...state,
                isLoading: false,
                loadingError: action.payload
            };
        }
        case VesselActionTypes.LoadSuccess: {
            // TODO create action for vessel list update and use it in scheduler
            if (state.ids.length > 0) {
                const changes = action.payload
                    .map(vessel => {
                        return updateVesselData(state, vessel);
                    });

                return {
                    ...adapter.updateMany(changes, state),
                    isLoaded: true,
                    isLoading: false,
                    loadingError: null
                };
            }

            return {
                ...adapter.addMany(action.payload, state),
                isLoaded: true,
                isLoading: false,
                loadingError: null
            };
        }
        case VesselActionTypes.LoadSingleSuccess: {
            const changes = updateVesselData(state, action.payload);

            return adapter.updateOne(changes, state);
        }
        case VesselActionTypes.LoadSingleFailure: {
            return state;
        }
        case VesselActionTypes.SaveVoyagePortsInfo: {
            // Remove this when a spinner is added while saving ports
            const vessel = state.entities[action.payload.vesselId];
            if (!vessel || !vessel.voyageInfo) {
                return state;
            }

            return adapter.updateOne({
                id: action.payload.vesselId,
                changes: {
                    voyageInfo: {
                        ...vessel.voyageInfo,
                        portOfDeparture: action.payload.portOfDeparture,
                        portOfDestination: action.payload.portOfDestination
                    }
                }
            }, state);
        }
        case VesselActionTypes.SaveVoyagePortsInfoSuccess: {
            const vessel = state.entities[action.payload.vesselId];
            if (!vessel) {
                return state;
            }

            return adapter.updateOne({
                id: action.payload.vesselId,
                changes: {
                    voyageInfo: action.payload.voyageInfo
                }
            }, state);
        }
        case VesselActionTypes.SaveVoyagePortsInfoFailure: {
            return state;
        }
        // is using only in rtm
        case VesselActionTypes.UpdateVesselPosition: {
            return adapter.updateOne({
                id: action.payload.vesselId,
                changes: {
                    freshestPosition: action.payload.freshestPosition
                }
            }, state);
        }
        case VesselActionTypes.UpdateVesselPositionFromIssue: {
            const vessel = getVesselByTcmsId(state, action.payload.tcmsId);
            if (!vessel || !action.payload.freshestPosition) {
                return state;
            }
            const freshestPosition = getLatestPosition(vessel, action.payload.freshestPosition);
            return adapter.updateOne({
                id: vessel.id,
                changes: {
                    freshestPosition: freshestPosition
                }
            }, state);
        }
        default: {
            return state;
        }
    }
}

function getVesselByTcmsId(state: State, tcmsId: string): Vessel {
    return Object.values(state.entities).find(v => v.tcmsId === tcmsId);
}

function getLatestPosition(vessel: Vessel, newPosition: Position): Position {
    if (!vessel.freshestPosition) {
        return newPosition;
    }
    return vessel
        && vessel.freshestPosition
        && newPosition
        && moment(newPosition.timestamp).isAfter(moment(vessel.freshestPosition.timestamp))
        ? { ...vessel.freshestPosition, ...newPosition }
        : { ...newPosition, ...vessel.freshestPosition };
}

function updateVesselData(state: State, vessel: Vessel): { id: number, changes: { freshestPosition: Position, voyageInfo: VoyageInfo }} {
    const prevVessel = getVesselByTcmsId(state, vessel.tcmsId);
    const freshestPosition = vessel.freshestPosition
        ? getLatestPosition(prevVessel, vessel.freshestPosition)
        : prevVessel.freshestPosition;
    return {
        id: vessel.id,
        changes: {
            freshestPosition: freshestPosition,
            voyageInfo: vessel.voyageInfo
        }
    };
}

