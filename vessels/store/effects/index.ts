import { VesselEffects } from './vessel.effects';
import { VesselGroupEffects } from './vessel-group.effects';
import { VesselIssueInfoEffects } from './vessel-issue-info.effects';
import { VesselConnectivityStatusEffects } from './vessel-connectivity-status.effects';
import { VesselFeaturesEffects } from './vessel-features.effects';
import { VesselInfoEffects } from './vessel-info.effects';
import { OwnAisDataEffects } from './own-ais-data.effects';
import { VesselTrackEffects } from './vessel-track.effects';
import { VesselTelemetryEffects } from './vessel-telemetry.effects';
import { VesselCloudEtaEffects } from './vessel-cloud-eta.effects';
import { VesselNextPassageEffects } from './vessel-next-passage.effects';
import { VesselEcdisDataEffects } from './vessel-ecdis-data.effects';
import { VesselRouteProgressEffects } from './vessel-route-progress.effects';
import { VesselScheduleEffects } from './vessel-schedule.effects';

export const VesselEffectsCollection = [
    VesselEffects,
    VesselGroupEffects,
    VesselFeaturesEffects,
    VesselIssueInfoEffects,
    VesselConnectivityStatusEffects,
    VesselInfoEffects,
    OwnAisDataEffects,
    VesselTrackEffects,
    VesselTelemetryEffects,
    VesselCloudEtaEffects,
    VesselNextPassageEffects,
    VesselEcdisDataEffects,
    VesselRouteProgressEffects,
    VesselScheduleEffects
];
