import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store, Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { mergeMap, map, catchError, withLatestFrom, filter, tap } from 'rxjs/operators';

import { VesselActionTypes, SaveVoyagePortsInfo, LoadingVesselsStarted, LoadVessel } from '../actions';
import { VesselService } from '../../services/vessel.service';
import * as fromVessels from '../reducers';

@Injectable()
export class VesselEffects {

    constructor(
        private store: Store<fromVessels.State>,
        private vesselService: VesselService,
        private actions$: Actions
    ) { }

    @Effect() loadVesselList$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselActionTypes.LoadAll),
            withLatestFrom(this.store.select<boolean>(fromVessels.getVesselsLoading)),
            filter(([action, isLoading]) => !isLoading),
            tap(() => this.store.dispatch(new LoadingVesselsStarted)),
            mergeMap(() => this.vesselService.loadVessels()
                .pipe(
                    map(data => ({ type: VesselActionTypes.LoadSuccess, payload: data })),
                    catchError((error) => of({ type: VesselActionTypes.LoadFailure, payload: error }))
                )
            )
        );

    @Effect() saveVoyagePorts$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselActionTypes.SaveVoyagePortsInfo),
            mergeMap((action: SaveVoyagePortsInfo) => {
                return this.vesselService.saveVoyagePortsInfo(action.payload)
                    .pipe(
                        map(data => ({
                            type: VesselActionTypes.SaveVoyagePortsInfoSuccess,
                            payload: { vesselId: action.payload.vesselId, voyageInfo: data }
                        })),
                        catchError(() => of({ type: VesselActionTypes.SaveVoyagePortsInfoFailure }))
                    );
            })
        );

    @Effect() loadVessel$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselActionTypes.LoadSingle),
            map((action: LoadVessel) => action.payload),
            mergeMap(vesselId => this.vesselService.loadVessel(vesselId)
                .pipe(
                    map(data => ({ type: VesselActionTypes.LoadSingleSuccess, payload: data })),
                    catchError(error => of({ type: VesselActionTypes.LoadSingleFailure, payload: error }))
                )
            )
        );

}
