import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { mergeMap, map, catchError } from 'rxjs/operators';

import { SaveVesselInfo, VesselInfoActionTypes, LoadVesselInfo } from '../actions';
import { VesselInfoService } from '../../services/vessel-info.service';

@Injectable()
export class VesselInfoEffects {

    constructor(
        private vesselInfoService: VesselInfoService,
        private actions$: Actions
    ) { }

    @Effect() load$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselInfoActionTypes.Load),
            mergeMap((action: LoadVesselInfo) =>
                this.vesselInfoService.loadVesselInfo(action.payload.tcmsId)
                    .pipe(
                        map(data => ({ type: VesselInfoActionTypes.LoadSuccess, payload: data })),
                        catchError(() => of({ type: VesselInfoActionTypes.LoadFailure }))
                    )
            )
        );

    @Effect() save$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselInfoActionTypes.Save),
            mergeMap((action: SaveVesselInfo) =>
                this.vesselInfoService.saveVesselInfo(action.payload)
                    .pipe(
                        map(data => ({ type: VesselInfoActionTypes.SaveSuccess, payload: data })),
                        catchError(error => of({ type: VesselInfoActionTypes.SaveFailure, payload: error }))
                    )
            )
        );

    @Effect() loadAll$: Observable<Action> = this.actions$
        .pipe(
            ofType(VesselInfoActionTypes.LoadAll),
            mergeMap(() =>
                this.vesselInfoService.loadAllVesselsInfo()
                    .pipe(
                        map(data => ({ type: VesselInfoActionTypes.LoadAllSuccess, payload: data })),
                        catchError(() => of({ type: VesselInfoActionTypes.LoadAllFailure }))
                    )
            )
        );

}
