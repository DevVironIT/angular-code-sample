import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

import { Vessel, VoyagePortsEditorData, VoyageInfo, Position } from '../../models';

export enum VesselActionTypes {
    LoadAll = '[Vessel] Load Vessel List',
    LoadingStarted = '[Vessel] Loading Vessels Started',
    LoadSuccess = '[Vessel] Load Vessel List Success',
    LoadFailure = '[Vessel] Load Vessel List Failure',

    LoadSingle = '[Vessel] Load Single Vessel',
    LoadSingleSuccess = '[Vessel] Load Single Vessel Success',
    LoadSingleFailure = '[Vessel] Load Single Vessel Failure',

    SaveVoyagePortsInfo = '[Vessel] Save Voyage Ports Info',
    SaveVoyagePortsInfoSuccess = '[Vessel] Save Voyage Ports Info Success',
    SaveVoyagePortsInfoFailure = '[Vessel] Save Voyage Ports Info Failure',

    UpdateVesselPosition = '[Vessel] Update Vessel Position',
    UpdateVesselPositionFromIssue = '[Vessel] Update Vessel Position From Issue',
}

export class LoadVessels implements Action {
    readonly type = VesselActionTypes.LoadAll;
}

export class LoadingVesselsStarted implements Action {
    readonly type = VesselActionTypes.LoadingStarted;
}

export class LoadVesselsSuccess implements Action {
    readonly type = VesselActionTypes.LoadSuccess;

    constructor(public payload: Vessel[]) { }
}

export class LoadVesselsFailure implements Action {
    readonly type = VesselActionTypes.LoadFailure;

    constructor(public payload: HttpErrorResponse) { }
}

export class LoadVessel implements Action {
    readonly type = VesselActionTypes.LoadSingle;

    constructor(public payload: number) { }
}

export class LoadVesselSuccess implements Action {
    readonly type = VesselActionTypes.LoadSingleSuccess;

    constructor(public payload: Vessel) { }
}

export class LoadVesselFailure implements Action {
    readonly type = VesselActionTypes.LoadSingleFailure;

    constructor(public payload: any) { }
}

export class SaveVoyagePortsInfo implements Action {
    readonly type = VesselActionTypes.SaveVoyagePortsInfo;

    constructor(public payload: VoyagePortsEditorData) { }
}

export class SaveVoyagePortsInfoSuccess implements Action {
    readonly type = VesselActionTypes.SaveVoyagePortsInfoSuccess;

    constructor(public payload: { vesselId: number, voyageInfo: VoyageInfo }) { }
}

export class SaveVoyagePortsInfoFailure implements Action {
    readonly type = VesselActionTypes.SaveVoyagePortsInfoFailure;

    constructor(public payload: any) { }
}

export class UpdateVesselPosition implements Action {
    readonly type = VesselActionTypes.UpdateVesselPosition;

    constructor(public payload: { freshestPosition: Position, vesselId: number }) { }
}

export class UpdateVesselPositionFromIssue implements Action {
    readonly type = VesselActionTypes.UpdateVesselPositionFromIssue;

    constructor(public payload: { tcmsId: string, freshestPosition: Position }) { }
}

export type VesselActions
    = LoadVessels
    | LoadingVesselsStarted
    | LoadVesselsSuccess
    | LoadVesselsFailure
    | LoadVesselSuccess
    | LoadVesselFailure
    | SaveVoyagePortsInfo
    | SaveVoyagePortsInfoSuccess
    | SaveVoyagePortsInfoFailure
    | UpdateVesselPosition
    | UpdateVesselPositionFromIssue;
