import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

import { VesselInfo, Vessel } from '../../models';

export enum VesselInfoActionTypes {
    Load = '[Vessel] Load Vessel Info',
    LoadSuccess = '[Vessel] Load Vessel Info Success',
    LoadFailure = '[Vessel] Load Vessel Info Failure',

    Save = '[Vessel] Save Vessel Info',
    SaveSuccess = '[Vessel] Save Vessel Info Success',
    SaveFailure = '[Vessel] Save Vessel Info Failure',

    LoadAll = '[Vessel] Load All Vessels Info',
    LoadAllSuccess = '[Vessel] Load All Vessels Info Success',
    LoadAllFailure = '[Vessel] Load All Vessels Info Failure',
}

export class LoadVesselInfo implements Action {
    readonly type = VesselInfoActionTypes.Load;

    constructor(public payload: Vessel) {}
}

export class LoadVesselInfoSuccess implements Action {
    readonly type = VesselInfoActionTypes.LoadSuccess;

    constructor(public payload: VesselInfo) { }
}

export class LoadVesselInfoFailure implements Action {
    readonly type = VesselInfoActionTypes.LoadFailure;

    constructor(public payload: HttpErrorResponse) { }
}

export class SaveVesselInfo implements Action {
    readonly type = VesselInfoActionTypes.Save;

    constructor(public payload: Partial<VesselInfo>) { }
}

export class SaveVesselInfoSuccess implements Action {
    readonly type = VesselInfoActionTypes.SaveSuccess;

    constructor(public payload: VesselInfo) { }
}

export class SaveVesselInfoFailure implements Action {
    readonly type = VesselInfoActionTypes.SaveFailure;

    constructor(public payload: HttpErrorResponse) { }
}

export class LoadAllVesselsInfo implements Action {
    readonly type = VesselInfoActionTypes.LoadAll;
}

export class LoadAllVesselsInfoSuccess implements Action {
    readonly type = VesselInfoActionTypes.LoadAllSuccess;

    constructor(public payload: VesselInfo[]) { }
}

export class LoadAllVesselsInfoFailure implements Action {
    readonly type = VesselInfoActionTypes.LoadAllFailure;

    constructor(public payload: any) { }
}

export type VesselInfoActions
    = LoadVesselInfo
    | LoadVesselInfoSuccess
    | LoadVesselInfoFailure
    | SaveVesselInfo
    | SaveVesselInfoSuccess
    | SaveVesselInfoFailure
    | LoadAllVesselsInfo
    | LoadAllVesselsInfoSuccess
    | LoadAllVesselsInfoFailure;
