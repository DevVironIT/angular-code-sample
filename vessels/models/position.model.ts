import { Port } from '@ports/models';
import { WeatherSynopticInfo, WeatherSensorInfo } from '@weather/models';

export interface Position {
    timestamp?: string;
    lat: number;
    lon: number;
    depth: number;
    heading: number;
    speed: number;
    course: number;
    weather?: WeatherSynopticInfo;
    sensorWeather?: WeatherSensorInfo;
    nearestPort?: Port;
    vesselName?: string;
}
