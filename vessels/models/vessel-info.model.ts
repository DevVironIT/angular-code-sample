import { Webcam } from './vessel-webcam.model';

export class VesselInfo {
    tcmsId: string;
    vesselType: string;
    length: number;
    beam: number;
    draught: number;
    masthead: number;
    capacity: number;
    maxWeight: number;
    minSpeed: number;
    averageSpeed: number;
    maxSpeed: number;
    sensorPosFromHead: number;
    sensorPosFromLeftBoard: number;
    webcams?: { cameras: Webcam[] };
}
