import { Position } from './position.model';
import { VesselIssueInfo } from './vessel-issue-info.model';
import { VoyageInfo } from './voyage-info.model';

export class Vessel {
    id: number;
    dmsClientId: string;
    tcmsId: string;
    name: string;
    imo: number;
    mmsi: number;
    freshestPosition: Position;
    issueInfo: VesselIssueInfo;
    voyageInfo: VoyageInfo;
    isOnline: boolean;
    hasRtmFeature: boolean;
    hasVoyagePlanningFeature: boolean;
    hasFuelFeature: boolean;
}
