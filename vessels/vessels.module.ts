import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'ngx-moment';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

import { reducers, VesselEffectsCollection } from './store';
import { SharedModule } from '@shared/shared.module';
import { IssuesModule } from '@issues/issues.module';
import { VesselInfoService, VesselService } from './services';
import { VesselGuard, VesselExistsGuard } from './guards';
import {
    VesselListComponent, VesselDetailsComponent, VesselPopupComponent,
    VesselStatusComponent, VoyagePortsInfoComponent, MonitoredRouteListComponent,
    IncompleteVesselListComponent, VesselDateComponent, OwnAisDataComponent, VesselTrackTooltipComponent,
    VesselPositionLastUpdateComponent, VesselTrackSettingsComponent, VesselRadarComponent, VesselWebcamComponent,
    VesselScheduleDialogComponent
} from './components';

@NgModule({
    imports: [
        MomentModule,
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        NgbModule,
        NgSelectModule,
        FormsModule,
        StoreModule.forFeature('vessels', reducers),
        EffectsModule.forFeature(VesselEffectsCollection),
        SharedModule,
        IssuesModule
    ],
    declarations: [
        VesselListComponent,
        VesselDetailsComponent,
        VesselPopupComponent,
        VesselStatusComponent,
        VoyagePortsInfoComponent,
        MonitoredRouteListComponent,
        IncompleteVesselListComponent,
        VesselDateComponent,
        OwnAisDataComponent,
        VesselTrackTooltipComponent,
        VesselPositionLastUpdateComponent,
        VesselTrackSettingsComponent,
        VesselRadarComponent,
        VesselWebcamComponent,
        VesselScheduleDialogComponent
    ],
    providers: [
        VesselService,
        VesselInfoService,
        VesselGuard,
        VesselExistsGuard
    ],
    entryComponents: [
        VesselPopupComponent,
        VesselTrackTooltipComponent,
        VesselTrackSettingsComponent,
        VesselScheduleDialogComponent
    ],
    exports: [
        VesselListComponent,
        VesselDetailsComponent,
        VesselPopupComponent,
        VesselStatusComponent,
        MonitoredRouteListComponent,
        IncompleteVesselListComponent,
        VesselDateComponent,
        OwnAisDataComponent,
        VesselTrackTooltipComponent,
        VesselPositionLastUpdateComponent,
        VesselRadarComponent,
        VesselWebcamComponent,
        VesselScheduleDialogComponent
    ]
})
export class VesselsModule { }
