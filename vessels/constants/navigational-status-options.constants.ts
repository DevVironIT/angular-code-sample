export const NavigationalStatusOptions = [
    'Underway using engine',
    'At anchor',
    'Not under command',
    'Restricted maneuverability',
    'Constrained by draught',
    'Moored',
    'Aground',
    'Fishing',
    'Underway sailing',
    'Towing astern',
    'Towing alongside',
    'Ais sart',
    'Undefined'
];
