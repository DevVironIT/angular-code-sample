import * as moment from 'moment';

import {
    Vessel, Position, VesselTelemetry,
    VesselCloudEta, VesselNextPassage,
    VesselEcdisData, VesselRouteProgress
} from '@vessels/models';
import { CoreFunctions } from '@core/utils';
import { OrganizationCode } from '@profile/models/organization-code.enum';
import { Issue } from '@issues/models';
import { Dictionary } from '@shared/models';
import { VesselPassagesInfo } from '@voyages/models';
import { RouteData } from '@routes/models';
import { RouteScheduleFunctions } from '@routes/utils';
import { SchedulePlannedParameters } from '@routes/constants';

export class VesselUtil {

    static getIconClassByMotion(vessel: Vessel): string {
        if (!vessel.isOnline) {
            return 'offline-vessel';
        }

        return 1 ? 'vessel-in-motion' : 'vessel-in-port';
    }

    static getIconClassByIssue(vessel: Vessel): string {
        return vessel.issueInfo ? vessel.issueInfo.topIssuePriority : '';
    }

    static getIconClassByBrand(brand: string): string {
        return brand === OrganizationCode.msc ? 'msc-vessel-icon' : 'vessel-icon';
    }

    static getHeading(pos: Position): number {
        return CoreFunctions.isNil(pos.heading) ? pos.course : pos.heading;
    }

    static isIconStatic(vessel: Vessel, brand = OrganizationCode.default): boolean {
        return brand === OrganizationCode.msc || !vessel.isOnline;
    }

    static formatVesselTelemetry(issue: Issue): VesselTelemetry {
        return <VesselTelemetry>{
            tcmsId: issue.vesselTcmsId,
            xtd: issue.complexIssueInfo.xtd
        };
    }


    static formatVesselPosition(complexIssueInfo: Dictionary<any>): Position {

        if (!complexIssueInfo['lon'] || !complexIssueInfo['lat'] || !complexIssueInfo['cog'] || !complexIssueInfo['trueheading']) {
            return null;
        }

        return <Position>{
            timestamp: moment(Number(complexIssueInfo['timestamp'] * 1000)).toISOString(),
            lat: VesselUtil.toNumberOrDefault(complexIssueInfo['lat']),
            lon: VesselUtil.toNumberOrDefault(complexIssueInfo['lon']),
            depth: VesselUtil.toNumberOrDefault(complexIssueInfo['depth']),
            speed: VesselUtil.toNumberOrDefault(complexIssueInfo['sog']),
            heading: VesselUtil.toNumberOrDefault(complexIssueInfo['trueheading']),
            course: VesselUtil.toNumberOrDefault(complexIssueInfo['cog']),
            sensorWeather: {
                airTemperature: VesselUtil.toNumberOrDefault(complexIssueInfo['airtemp']),
                humidity: VesselUtil.toNumberOrDefault(complexIssueInfo['humidity']),
                pressure: VesselUtil.toNumberOrDefault(complexIssueInfo['pressure']),
                waterTemperature: VesselUtil.toNumberOrDefault(complexIssueInfo['watertemp']),
                windDirection: VesselUtil.toNumberOrDefault(complexIssueInfo['winddirection']),
                windSpeed: VesselUtil.toNumberOrDefault(complexIssueInfo['windspeed']),
            }
        };
    }

    static formatVesselCloudEta(issue: Issue): VesselCloudEta {
        return <VesselCloudEta> {
            tcmsId: issue.vesselTcmsId,
            eta: issue.complexIssueInfo.eta,
            timestamp: moment(Number(issue.complexIssueInfo['timestamp'] * 1000)).toISOString()
        };
    }

    static isTimestampRelevant(timestamp: string, relevancePeriod = 5): boolean {
        return Math.abs(moment().diff(timestamp, 'minutes')) <= relevancePeriod;
    }

    static toNumberOrDefault(value: string): number {
        if (!value) {
            return null;
        }

        return Number(value);
    }

    static formatVesselNextPassage(issue: Issue): VesselNextPassage {
        return <VesselNextPassage> {
            tcmsId: issue.vesselTcmsId,
            name: issue.complexIssueInfo.name,
            eta: issue.complexIssueInfo.eta,
            pta: issue.complexIssueInfo.pta
        };
    }

    static formatVesselPassagesInfo(vessel: Vessel, routeData: RouteData, nextPassage: VesselNextPassage): VesselPassagesInfo {
        return {
            tcmsId: vessel.tcmsId,
            dmsId: vessel.dmsClientId,
            name: vessel.name,
            current: routeData ? {
                name: routeData.route?.info?.routeName,
                eta: vessel.voyageInfo?.cloudEta?.eta
                    || vessel.voyageInfo?.ecdisData?.calculatedEta
                    || vessel.voyageInfo?.ecdisData?.scheduledEta,
                pta: RouteScheduleFunctions.getPtaOrPtdForWaypoint(
                    routeData,
                    RouteScheduleFunctions.getRouteLastWaypoint(routeData),
                    SchedulePlannedParameters.pta)
            } : null,
            next: nextPassage?.name ? {
                name: nextPassage.name,
                eta: nextPassage.eta,
                pta: nextPassage.pta
            } : null
        };

    }

    static formatVesselEcdisData(issue: Issue): VesselEcdisData {
        return {
            tcmsId: issue.vesselTcmsId,
            calculatedEta: moment.unix(issue.complexIssueInfo.calculated_eta).format(),
            scheduledEta: moment.unix(issue.complexIssueInfo.scheduled_eta).format()
        };
    }

    static getCoveredDistancePercentage(total: number, toGo: number): number {
        return Math.ceil(100 * ((total - toGo) / total));
    }

    static formatVesselRouteProgress(issue: Issue): VesselRouteProgress {
        return {
            tcmsId: issue.vesselTcmsId,
            distanceInMeters: issue.complexIssueInfo.total,
            remainingDistanceInMeters: issue.complexIssueInfo.togo,
            coveredDistancePercentage: VesselUtil.getCoveredDistancePercentage(issue.complexIssueInfo.total, issue.complexIssueInfo.togo)
        };
    }

    static formatVesselIssue(issue: any, vessels: Vessel[]): Issue {
        try {
            if (!issue.vesselTcmsId) {
                issue = {
                    ...issue,
                    vesselTcmsId: vessels.find(v => v.dmsClientId === issue.vessel.id).tcmsId
                }
            }
            return issue;
        } catch (e) {
            console.log('Detected error, Retrying attemp.')
            throw(e)
        }
    }

}
