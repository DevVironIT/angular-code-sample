import { environment } from '../../../environments/environment';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { VesselInfo } from '../models';

@Injectable()
export class VesselInfoService {

    constructor(private httpClient: HttpClient) { }

    loadAllVesselsInfo(): Observable<VesselInfo[]> {
        return this.httpClient.get<VesselInfo[]>(environment.api.profiles.getVessels);
    }

    loadVesselInfo(tcmsId: string): Observable<VesselInfo> {
        const url = `${environment.api.profiles.getVessels}/${tcmsId}`;
        return this.httpClient.get<VesselInfo>(url);
    }

    saveVesselInfo(vesselInfo: Partial<VesselInfo>): Observable<VesselInfo> {
        const url = `${environment.api.profiles.getVessels}/${vesselInfo.tcmsId}`;
        return this.httpClient.post<VesselInfo>(url, vesselInfo);
    }

}
