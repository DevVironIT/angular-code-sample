import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { FeatureCollection } from 'geojson';
import { Observable } from 'rxjs';

import { Vessel, VesselGroup, VoyageInfo, VoyagePortsEditorData, VesselFeatures, Position } from '../models';

@Injectable()
export class VesselService {

    constructor(private httpClient: HttpClient) { }

    loadVessels(): Observable<Vessel[]> {
        return this.httpClient
            .get<Vessel[]>(environment.api.vessels.getVessels);
    }

    loadVesselGroups(): Observable<VesselGroup[]> {
        return this.httpClient.get<VesselGroup[]>(environment.api.vessels.getGroups);
    }

    loadVesselFeatures(): Observable<VesselFeatures[]> {
        return this.httpClient.get<VesselFeatures[]>(environment.api.vessels.getFeatures);
    }

    saveVoyagePortsInfo(data: VoyagePortsEditorData): Observable<VoyageInfo> {
        const url = environment.api.vessels.savePortsInfo.replace('{routeId}', String(data.routeId));
        return this.httpClient.put<VoyageInfo>(url, {
            portOfDeparture: data.portOfDeparture,
            portOfDestination: data.portOfDestination
        });
    }

    loadVesselTrack(vesselId: number, periodInDays = 2, format = 'geoJson'): Observable<FeatureCollection> {
        const trackPeriod = moment.duration(periodInDays, 'days').asHours();
        const requestUrl =`${environment.api.vessels.endpoint}/${vesselId}/loadtrack?trackPeriodHours=${trackPeriod}&format=${format}`;
        return this.httpClient.get<FeatureCollection>(requestUrl);
    }

    loadVessel(vesselId: number): Observable<Vessel> {
        return this.httpClient.get<Vessel>(`${environment.api.vessels.endpoint}/${vesselId}`);
    }

    loadVesselTrackPoint(vesselId: number, timestamp: string): Observable<Position> {
        return this.httpClient.get<Position>(`${environment.api.vessels.endpoint}/${vesselId}/trackPoints?timestamp=${timestamp}`);
    }
}
